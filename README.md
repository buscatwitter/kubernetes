# Kubernetes

Neste repositório temos todos os arquivos necessários para o deploy de todo o ecosistema (Backend, Frontend e Banco de Dados) em um ambiente gerenciador de containers, Kubernetes.

**Tecnologias**

- Kubernetes

**Pré-Requisitos**

- Minikube (Ambiente local e testes)
- Kubectl
- Nuvem Pública (Ambiente Produtivo)

# Exemplo Set para um cluster no Google
``` bash
# Set de Ambiente
gcloud container clusters get-credentials desafio-twitter --zone southamerica-east1-a --project rare-shadow-25971
```

# Deploy Banco de Dados

Para criação do objeto Pod e Service (LoadBalancer) do banco de dados, execute:

``` bash
# Objeto Pod
kubectl create -f database/database-pod.yml

# Objeto Service
kubectl create -f database/database-service.yml
```

# Deploy Backend

Para criação do objeto Deployment e Service (LoadBalance) do Backend, execute:

``` bash
# Objeto Deployment
kubectl create -f backend-app/backend-deployment.yml

# Objeto Service
kubectl create -f backend-app/backend-service.yml
```

# Deploy Frontend
Para criação do objeto Deployment e Service (LoadBalance) do Frontend, execute:

``` bash
# Objeto Deployment
kubectl create -f frontend-app/frontend-deployment.yml

# Objeto Service
kubectl create -f frontend-app/frontend-service.yml
```

# Verificando Pods, Deployments e Services

Para verificar se os objetos estão em execução, execute:

``` bash
# Pods
kubectl get pods

# Deployments
kubectl get deployments

# Services
kubectl get services
```

# Recuperar a URL de Serviços (Endereço LoadBalancer)

Para verificar as url`s de serviços geradas, execute:


``` bash
# Verificar url backend
minikube service backend-service --url

# Verificar url frontend
minikube service frontend-service --url

```
